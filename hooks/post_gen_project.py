import os

if __name__ == "__main__":
  # cookiecutter does not seem to be able to insert an envrionment variable,
  # so we manually check whether the URL has been inserted and insert it.
  with open('README.md') as readme:
    start = readme.read(4)
  if start != 'http':
    with open('README.md') as infile:
      contents = infile.read()
    with open('README.md', 'w') as outfile:
      # CI_PAGES_DOMAIN does not include the group, e.g. just gitlab.io
      print(os.environ['CI_PAGES_URL'].replace(os.environ['CI_PROJECT_NAME'], os.environ['repo_name']).replace(os.environ['CI_PROJECT_NAMESPACE'], os.environ['NAMESPACE_COOKIECUT_TO']), file=outfile)
      # Since README.md is Markdown, we need a second newline to actually break the line.
      print('', file=outfile)
      outfile.write(contents)

{% if cookiecutter.include_R == "no" %}
  os.unlink('Rnotebook.ipynb')
{% endif %}

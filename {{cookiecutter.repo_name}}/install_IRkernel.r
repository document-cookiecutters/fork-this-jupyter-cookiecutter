if(!require(digest)) install.packages('digest', repos='http://cran.us.r-project.org')
if(!require(htmltools)) install.packages('htmltools', repos='http://cran.us.r-project.org')
if(!require(IRkernel)) install.packages('IRkernel', repos='http://cran.us.r-project.org')
IRkernel::installspec()

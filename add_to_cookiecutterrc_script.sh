if [ -z ${DOWNSTREAM_PAGES_URL+ABC} ]; then
  TOP_LEVEL_NAMESPACE=$(echo $NAMESPACE_COOKIECUT_TO | cut -d '/' -f 1)
  REST_OF_NAMESPACE=${NAMESPACE_COOKIECUT_TO/$TOP_LEVEL_NAMESPACE}
  export DOWNSTREAM_PAGES_URL=http://$TOP_LEVEL_NAMESPACE.${CI_PAGES_DOMAIN}${REST_OF_NAMESPACE}/$(echo $repo_name | tr '[:upper:]' '[:lower:]')
fi
printf "  DOWNSTREAM_PAGES_URL:" >> $HOME/.cookiecutterrc
echo " $DOWNSTREAM_PAGES_URL" >> $HOME/.cookiecutterrc
if ! [ -z ${include_R+ABC} ]; then
  printf "  include_R:" >> $HOME/.cookiecutterrc
  echo " $include_R" >> $HOME/.cookiecutterrc
fi

You can run this cookiecutter manually on your own computer.

To run this cookiecutter through the gitlab-runner, you will need to give it an SSH_PRIVATE_DEPLOY_KEY to access the repo you wish to cookiecut to.

This will work even if, for example, you don't have a working Python installation. You can just use the cookiecutter Docker image.

Unfortunately, only an instance administrator can create a public deploy key (https://docs.gitlab.com/ee/user/project/deploy_keys/#public-deploy-keys), so you will need to have your own individual deploy key, which means you will need to fork this repository and set your deploy key in the CI/CD variables.

If you don't already have a deploy key, or you don't wish to provide generate one with `ssh-keygen -t ed25519 -C "Key for deploying Jupyter cookiecutter to child repo."`
Rather than overwrite your main `~/.ssh/id_ed25519`, you probably want to just create a local file `id_ed25519` (with corresponding `.pub`) in the current directory. You'll be able to discard both files once this process is done.

In Settings -> CI/CD of your fork of this repo, set `SSH_PRIVATE_DEPLOY_KEY` to the contents of that `id_ed25519` file.
You will also need to enable a gitlab-runner.

In Settings -> Repository of the repo you wish to cookiecut to, add a deploy key "Key for deploying Jupyter cookiecutter to child repo." with the contents of `id_ed25519.pub`.

Finally, on your fork of this repo, click CI/CD, click Run Pipeline, fill in the `DEFAULT_NAMESPACE_COOKIECUT_TO` and `default_repo_name` values, and click the new Run Pipeline button.
